// includes, system
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// includes, project
#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>
#include "common/inc/helper_cuda.h"
#include "common/inc/helper_functions.h"
#include <GL/glut.h>
const int N = 1024;
const int M = 256;
const int vector_size = 15;
const int batch_size = 17476;



// to hold result from gpu computation
static float2 * d_input;

static float h_eyes[N*M];
static float * d_eyes;
static float h_thread_row_constants[N*M];
static float * d_constants;
static cufftHandle plan;


// reduction coeff
static float h_constants[15] = {1, 0.2, 0.04, 0.008, 0.0016, 0.00032,
0.000064, 0.0000128, 0.00000256, 0.000000512, 0.000000102,
0.00000002, 0.000000004, 0.0000000008192, 0.00000000016384};

// mapping coeff and pinter for gpu mem
static float2 h_map[5] = {{0.0, 0.0},{1.0, 0.0},{-1.0, 0.0},{0.0, 1.0},{0.0, -1.0}};

static float2 * d_map;

// get the kernel ready bro
static dim3 threads_per_block(N, 1);
static dim3 blocks_per_grid(M,1);


float2 res_output[N*M];




__global__ void kernel(float2 * result, float * init_eyes, float * init_coeff, float2 * map) {

int index_entry = blockIdx.x * blockDim.x + threadIdx.x;

	if (index_entry < N*M) {
		result[index_entry] = map[(int)floor(fmod(init_eyes[index_entry]*init_coeff[index_entry], (float)5))];
		__syncthreads();
	}
	init_eyes[index_entry] += 17476;
	__syncthreads();
}


// store iteration values in thread length vector
void init_eyes(float * arr, int a, int b){
	int cnt = 0;
	int fill_val = 0;
	int c = (int)a/b;
	for (int i = 0; i < a; i++) {
		if (cnt == b) {
			fill_val++;
			cnt = 0;
			if (fill_val == c) {
				fill_val = 0;
			}
		}
		arr[i] = fill_val;
		cnt++;
	}
}

// store coefficients in thread length vector
void init_reduction_coeff(float * arr, float * consts, int a, int b){
//int c = (int)a/b;
	int d = a%b;
	for (int i = 0; i < a; i++) {
		if (i >= (b-d-1)) {
			arr[i] = 0;
		}
		arr[i] = consts[i%vector_size];
	}
}

void compute_data() {



	// enumerate the function space
	kernel<<<blocks_per_grid, threads_per_block>>>(d_input, d_eyes, d_constants, d_map);
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize1\n");
	}

	// compute fourier transform of the function space
	checkCudaErrors(cufftExecC2C(plan, reinterpret_cast<float2*>(d_input), reinterpret_cast<float2 *>(d_input), CUFFT_FORWARD));
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize2\n");
	}
	
	// copy result to host
	checkCudaErrors(cudaMemcpy(res_output, d_input, M*N*sizeof(float2), cudaMemcpyDeviceToHost));
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize23\n");
	}

}

void cuda_inits() {

    // init host arrays to be transferred to gpu
    init_eyes(h_eyes, N*M, vector_size);
    init_reduction_coeff(h_thread_row_constants, h_constants, N*M, vector_size);
	
    // make FFT plan
    checkCudaErrors(cufftPlan1d(&plan, vector_size, CUFFT_C2C, batch_size));

    // allocate memory
    checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_input),M*N*sizeof(float2)));
	checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_map),5*sizeof(float2)));
    checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_eyes),N*M*sizeof(int)));
    checkCudaErrors(cudaMalloc(reinterpret_cast<cufftHandle**>(&d_constants), N*M*sizeof(float)));

    // transfer allocated memory over to the gpu
    checkCudaErrors(cudaMemcpy(d_map, h_map, 5*sizeof(float2),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_eyes, h_eyes, N*M*sizeof(float),cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_constants, h_thread_row_constants,M*N*sizeof(float), cudaMemcpyHostToDevice));

}


int main(int argc, char** argv) {

	cuda_inits();
	// init_array_of_ones(map_output);

	compute_data();

	// write the vectors to file
	int cnt = 1;
	FILE *fp;
	fp = fopen("eigen_values.txt", "w+");
	for (int i = 0; i < N*M; i++) {
		fprintf(fp, "(%f,%f)", res_output[i].x, res_output[i].y);
	if (cnt == 15) {
		fprintf(fp, "\n%i\n", i/15);
		cnt = 0;
	}
		cnt++;
	}



	checkCudaErrors(cudaFree(d_constants));
	checkCudaErrors(cudaFree(d_input));
	checkCudaErrors(cudaFree(d_eyes));
}
