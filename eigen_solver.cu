// includes, system
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// includes, project
#include <cuda_runtime.h>
#include <cufft.h>
#include <cufftXt.h>
#include "common/inc/helper_cuda.h"
#include "common/inc/helper_functions.h"
#include <GL/glut.h>
const int N = 1024;
const int M = 256;
const int vector_size = 15;
const int batch_size = 17476;
const int pixel_width = 600;
const int pixel_height = 600;
float2 number;

static float * d_pmap;
static float res_pmap[N*M];

static float count = 0;

static uint8_t r = 0;
static float red = r;
static uint8_t g = 0;
static float green = g;
static uint8_t b = 0;
static float blue = b;

// to hold result from gpu computation
static float2 * d_input;
//float2 res_output[N*M];
// vector to store initial shared mem
static float h_eyes[N*M];
static float * d_eyes;
static float h_thread_row_constants[N*M];
static float * d_constants;
static cufftHandle plan;


// reduction coeff
static float h_constants[15] = {1, 0.2, 0.04, 0.008, 0.0016, 0.00032,
0.000064, 0.0000128, 0.00000256, 0.000000512, 0.000000102,
0.00000002, 0.000000004, 0.0000000008192, 0.00000000016384};
// mapping coeff and pinter for gpu mem
static float2 h_map[5] = {{0.0, 0.0},{1.0, 0.0},{-1.0, 0.0},{0.0, 1.0},{0.0, -1.0}};
/*
h_map[0].x = 0;
h_map[0].y = 0;
h_map[1].x = 1;
h_map[1].y = 0;
h_map[2].x = -1;
h_map[2].y = 0;
h_map[3].x = 0;
h_map[3].y = 1;
h_map[4].x = 0;
h_map[4].y = -1;
*/
static float2 * d_map;

// get the kernel ready bro
static dim3 threads_per_block(N, 1);
static dim3 blocks_per_grid(M,1);

struct point {
GLfloat x;
GLfloat y;
};

point graph[M*N];

int point_color;
int indx;

float2 res_output[N*M];
float2 h_pixel_coords[N*M];
float2 * d_pixel_coords;
float map_output[pixel_width*pixel_height];
float map_output_2[pixel_width*pixel_height];
// create the radix count-
__global__ void kernel(float2 * result, float * init_eyes, float * init_coeff, float2 * map) {

int index_entry = blockIdx.x * blockDim.x + threadIdx.x;

	if (index_entry < N*M) {
		result[index_entry] = map[(int)floor(fmod(init_eyes[index_entry]*init_coeff[index_entry], (float)5))];
		__syncthreads();
	}
	init_eyes[index_entry] += 17476;
	__syncthreads();
}

__global__ void complex_to_pixel(float2 * a, float * c, float2 * coords)  {
	
	int index_entry = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (index_entry < N*M) {
		c[index_entry] = roundf((300.0 - 40.0*a[index_entry].y)*pixel_width + (300.0 + 40.0*a[index_entry].x));
		coords[index_entry].x = (300.0 - 40.0*a[index_entry].y)*pixel_width;
		coords[index_entry].y = (300.0 + 40.0*a[index_entry].x);
	}
	__syncthreads();
}

// set config type attributes for window display
void my_init() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glColor3f(0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(1.05);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 
	gluOrtho2D(-600.0, 600.0, -600.0, 600.0);
}

// to pass into glutinit window function
void display() {
	
	glViewport(0.0, 0.0, 600.0, 600.0);
	//glColor3f(fmod(i, 255), 0.0, 0.0);
	glBegin(GL_POINTS);
	for (int i = 0; i < M*N; i++) {
		indx = (int)res_pmap[i];
		point_color = abs(16777216 - map_output[abs(indx)]);
		point_color = point_color % 16777216;
		red = (point_color & 0xFF000000) >> 24;
		green = (point_color & 0x00FF0000) >> 16;
		blue = (point_color & 0x0000FF00) >> 8;
		glColor3ub (red, green, blue);	
	
		//printf("%f\n", a);
		graph[i].x = 60*res_output[i].x;
		graph[i].y = 60*res_output[i].y;
		//graph[i].x = h_pixel_coords[i].x;
		//graph[i].y = h_pixel_coords[i].y;
		//printf("(%f,%f)\n", graph[i].x, graph[i].y);
		glVertex2f(graph[i].x, graph[i].y);
	
	}
	glEnd();
	glFlush();
}

void init_array_of_ones(float * result) {

	for (int i = 0; i < pixel_width*pixel_width; i++) {
		result[i] = 0;
	}


}

// store iteration values in thread length vector
void init_eyes(float * arr, int a, int b){
	int cnt = 0;
	int fill_val = 0;
	int c = (int)a/b;
	for (int i = 0; i < a; i++) {
		if (cnt == b) {
			fill_val++;
			cnt = 0;
			if (fill_val == c) {
				fill_val = 0;
			}
		}
		arr[i] = fill_val;
		cnt++;
	}
}

// store coefficients in thread length vector
void init_reduction_coeff(float * arr, float * consts, int a, int b){
//int c = (int)a/b;
	int d = a%b;
	for (int i = 0; i < a; i++) {
		if (i >= (b-d-1)) {
			arr[i] = 0;
		}
		arr[i] = consts[i%vector_size];
	}
}

void compute_data() {




	kernel<<<blocks_per_grid, threads_per_block>>>(d_input, d_eyes, d_constants, d_map);
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize1\n");
	}
	checkCudaErrors(cufftExecC2C(plan, reinterpret_cast<float2*>(d_input), reinterpret_cast<float2 *>(d_input), CUFFT_FORWARD));
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize2\n");
	}
	complex_to_pixel<<<blocks_per_grid, threads_per_block>>>(d_input, d_pmap, d_pixel_coords);
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize23\n");
	}
	checkCudaErrors(cudaMemcpy(res_output, d_input, M*N*sizeof(float2), cudaMemcpyDeviceToHost));
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize23\n");
	}
	checkCudaErrors(cudaMemcpy(res_pmap, d_pmap, M*N*sizeof(float), cudaMemcpyDeviceToHost));	
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize5\n");
	}
	checkCudaErrors(cudaMemcpy(h_pixel_coords, d_pixel_coords, M*N*sizeof(float), cudaMemcpyDeviceToHost));	
	if (cudaDeviceSynchronize() != cudaSuccess) {
		fprintf(stderr, "Cuda error: failed to synchronize5\n");
	}
	for (int j = 0; j < N*M; j++) {
		indx = (int)res_pmap[j];
		
		map_output[abs(indx)] = map_output[abs(indx)] + 1;
	}
	glutPostRedisplay();
	glutDisplayFunc(display);
	glFlush();
	count++;
	printf("%f\n", count);

}

void cuda_inits() {

// init host arrays to be transferred to gpu
init_eyes(h_eyes, N*M, vector_size);
init_reduction_coeff(h_thread_row_constants, h_constants, N*M, vector_size);
// make FFT plan
checkCudaErrors(cufftPlan1d(&plan, vector_size, CUFFT_C2C, batch_size));
// allocate memory
checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_pixel_coords),N*M*sizeof(float2)));
checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_pmap),N*M*sizeof(float)));
checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_map),5*sizeof(float2)));
checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_input),M*N*sizeof(float2)));
checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&d_eyes),N*M*sizeof(int)));
checkCudaErrors(cudaMalloc(reinterpret_cast<cufftHandle**>(&d_constants), N*M*sizeof(float)));

// transfer allocated memory over to the gpu
checkCudaErrors(cudaMemcpy(d_pixel_coords, h_pixel_coords, M*N*sizeof(float2),cudaMemcpyHostToDevice));
checkCudaErrors(cudaMemcpy(d_map, h_map, 5*sizeof(float2),cudaMemcpyHostToDevice));
checkCudaErrors(cudaMemcpy(d_eyes, h_eyes, N*M*sizeof(float),cudaMemcpyHostToDevice));
//checkCudaErrors(cudaMemcpy(d_pmap, h_pmap, N*M*sizeof(float),cudaMemcpyHostToDevice));
checkCudaErrors(cudaMemcpy(d_constants, h_thread_row_constants,M*N*sizeof(float), cudaMemcpyHostToDevice));


}


int main(int argc, char** argv) {

cuda_inits();
init_array_of_ones(map_output);

glutInit(&argc, argv);
glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
glutInitWindowSize(600,600);
glutInitWindowPosition(0,0);
glutCreateWindow("Points");
glutIdleFunc(compute_data);
my_init();
glutMainLoop();



//float n;



// create graphic with opengl
/*glutInit(&argc, argv);
glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
glutInitWindowSize(700,700);
glutInitWindowPosition(0,0);
glutCreateWindow("Points");
glutDisplayFunc(display);
my_init();
glutMainLoop();
*/

// write the vectors to file
/*int cnt = 1;
FILE *fp;
fp = fopen("eigen_values.txt", "w+");
for (int i = 0; i < N*M; i++) {
fprintf(fp, "(%f,%f)", res_output[i].x, res_output[i].y);
if (cnt == 15) {
fprintf(fp, "\n%i\n", i/15);
cnt = 0;
}
cnt++;
}*/
// free gpu memory
checkCudaErrors(cudaFree(d_constants));
checkCudaErrors(cudaFree(d_input));
checkCudaErrors(cudaFree(d_eyes));
}
