# Cuda Eigenvalue Solver


### To run ###
assuming cuda capable gpu and the sdk is installed:

nvcc eigen_solver.cu -lcufft -lglut -lGL -lGLU              
nvcc fourier_eigen.cu -lcufft


### fourier_eigen.cu ###

This programs main purpose is to take a vector of length 15 and a set of coefficients {-1, 0, 1, i, -i}. 

#### step 1 - Enumerate all possible vectors in the degree 15 function space ####

```c++
__global__ void kernel(float2 * result, float * init_eyes, float * init_coeff, float2 * map) {

    int index_entry = blockIdx.x * blockDim.x + threadIdx.x;

    if (index_entry < N*M) {
        result[index_entry] = map[(int)floor(fmod(init_eyes[index_entry]*init_coeff[index_entry], (float)5))];
        __syncthreads();
    }
        init_eyes[index_entry] += 17476;
        __syncthreads();
}
```
The kernel executes on an array of threads of size 1024x256 (262144), this is 256 blocks of size 1024 threads. This large array is partitioned into 17476 vectors of size 15, the remiander spots in the array that dont correspond to a vector are filled with zeros. The permutations of the vectors are computed by base 5 counting the vector. Ex:     
 
                                                 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},                                                       
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},                                            
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},                                                                                 
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,3},                                                                                             
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,4},                                     
{0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},                                                        
...                                                                            
{4,4,4,4,4,4,4,4,4,4,4,4,4,4,4}                      

A hash map is used to map one of the 5 {-1, 0, 1, i, -i} values to the numbers in the vector. 

To count up on the vector you produce vectors with this structure. If you had more than if you had 4 coefficients and size 10 vector it would be i/4^n, n from 0 to 9.
```
{i/5^0 /5^1, i/5^2, i/5^3, i/5^4, i/5^5, i/5^6, i/5^7, i/5^8, i/5^9, i/5^10, i/5^11, i/5^12, i/5^13, i/5^14}
```
Here i ranges from your coeffcient size to your vector lenth, This case 5^15 (very large number). Which here reduces to nice coefficient values you can just multiply your loop index values 
```
{1, 0.2, 0.04, 0.008, 0.0016, 0.00032,0.000064, 0.0000128, 0.00000256, 0.000000512, 0.000000102, 0.00000002, 0.000000004, 0.0000000008192, 0.00000000016384}
```

#### step 2 - solve for the all the eigenvalues ####

Once the array of 1024x256 values have been mapped to their values in the function space the fast fourier transform is taken of this on the gpu. The 1024x256 array of values was partitioned into 17476 size 15 vectors so we can input it as our batch size for the fft. CUFFT was used. 

```c++
checkCudaErrors(cufftPlan1d(&plan, vector_size, CUFFT_C2C, batch_size));
checkCudaErrors(cufftExecC2C(plan, reinterpret_cast<float2*>(d_input), reinterpret_cast<float2 *>(d_input), CUFFT_FORWARD));
```

You then are left with the eigenvalues of the and printed to a text file or ready for whatever use you have. This comes from a special property of circulant/toeplitz matrices.


### eigen_solver.cu ###

This program is to plot the eigen values and structure the data in whatever way you want. Currently it is using openGL to render eigenvalues points to a window. You can process the points with varying color intensities based on how you partition the pixel map. A printed example file output of the eigen values is in eigenplot.txt.

Cool patterns can show up with analyzing eigen values:

![Alt text](./eigenplot.png?raw=true "Plotted eigen values")
